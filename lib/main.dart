import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:permission_handler/permission_handler.dart';
import './home/home.dart';
import './appbaronly.dart';

void main() {
  timeDilation = 2;
  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      systemNavigationBarColor: Colors.transparent,
    ),
  );
  runApp(MaterialApp(
    title: 'Navigation Basics',
    home: LoginPage(),
  ));
}

class LoginPage extends StatefulWidget {
  State<StatefulWidget> createState() {
    return _LoginScreen();
  }
}

class _LoginScreen extends State<LoginPage> {
  @override
  void initState() {
    super.initState();
    _checkpermissions();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _checkpermissions() async {
    if (await Permission.microphone.request().isGranted) {
      // Either the permission was already granted before or the user just granted it.
    } else {
      //open app settings to give app permissions
      showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return AlertDialog(
            title: Text("Use Microphone"),
            content: Text(
                "The visualizer engine used in this application requires you to grant it permissions to access the microphone failure to do so might result in errors or unexpected behaviour."),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              FlatButton(
                child: Text("Settings"),
                onPressed: () {
                  openAppSettings();
                },
              ),
              FlatButton(
                child: Text("Close"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    void _login() {
      print("login");
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Home("Accounts")),
      );
    }

    return MaterialApp(
      theme: ThemeData(
        brightness: Brightness.light,
        accentColor: Colors.orange[400],
        primaryColor: Colors.orange[400],
        fontFamily: 'NanumGothic',
        textTheme: TextTheme(
          headline: TextStyle(
              color: Colors.black38,
              fontFamily: 'Monoton',
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.normal),
          title: TextStyle(
              color: Colors.black38,
              fontFamily: 'NanumGothic',
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.bold),
          body1: TextStyle(
              color: Colors.orange[400],
              fontFamily: 'Monoton',
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.normal),
        ),
      ),
      home: Scaffold(
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(60),
            child: AppBarWidget("Infinity Realm")),
        body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: <Widget>[
              Container(
                width: double.infinity,
                height: 70,
                margin: EdgeInsets.only(bottom: 50, top: 50),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("Infinity",
                        style: TextStyle(fontSize: 40),
                        textAlign: TextAlign.center),
                    ScaleAnimatedTextKit(
                        onTap: () {
                          print("Tap Event");
                        },
                        duration: Duration(milliseconds: 2000),
                        isRepeatingAnimation: true,
                        totalRepeatCount: 1000000000,
                        text: ["Realm", "Admin"],
                        textStyle:
                            TextStyle(fontSize: 40, fontFamily: "Monoton"),
                        textAlign: TextAlign.center,
                        alignment: AlignmentDirectional
                            .topStart // or Alignment.topLeft
                        ),
                  ],
                ),
              ),
              Container(
                width: double.infinity,
                height: 50,
                margin: EdgeInsets.all(20),
                child: TextField(
                  obscureText: false,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.elliptical(30, 30),
                          topRight: Radius.elliptical(30, 30),
                        ),
                      ),
                      labelText: "Enter you\'re employee id"),
                ),
              ),
              Container(
                width: double.infinity,
                height: 50,
                margin: EdgeInsets.all(20),
                child: TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.elliptical(30, 30),
                          topRight: Radius.elliptical(30, 30),
                        ),
                      ),
                      labelText: "Enter you\'re password"),
                ),
              ),
              FlatButton(
                onPressed: _login,
                child: Text(
                  "Log In",
                  style: TextStyle(
                      color: Colors.orange[400], fontFamily: 'NanumGothic'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
