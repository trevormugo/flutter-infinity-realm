import 'package:flutter/material.dart';

class CuratedObj {
  CuratedObj(
      {@required this.displayname,
      @required this.thumbnail,
      @required this.curator,
      @required this.type})
      : assert(displayname != null &&
            thumbnail != null &&
            curator != null &&
            type != null);
  String displayname;
  String thumbnail;
  String curator;
  String type;
}
