import 'package:flutter/material.dart';

class MultiUploadRequest {
  MultiUploadRequest({
    @required this.files,
    @required this.curationId,
    @required this.type,
  }) : assert(files != null && curationId != null && type != null);
  List files;
  String curationId;
  String type;
}

class OneUploadRequest {
  OneUploadRequest(
      {@required this.displayname,
      @required this.uploader,
      @required this.downloadable,
      @required this.private,
      @required this.musicfile,
      @required this.thumbnail,
      this.thumbnailbox})
      : assert(displayname != null &&
            uploader != null &&
            downloadable != null &&
            private != null &&
            musicfile != null &&
            thumbnail != null);

  String displayname;
  String uploader;
  String downloadable;
  String private;
  String musicfile;
  String thumbnail;
  Widget thumbnailbox;
}
