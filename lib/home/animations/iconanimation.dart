import 'package:flutter/material.dart';

class IconAnimation extends StatefulWidget {
  IconAnimation(
      {@required this.color,
      @required this.icon,
      @required this.shouldanimate});
  final Color color;
  final IconData icon;
  final bool shouldanimate;
  State<StatefulWidget> createState() {
    return IconAnimationState();
  }
}

class IconAnimationState extends State<IconAnimation>
    with TickerProviderStateMixin {
  AnimationController _fadeoutanimationcontroller;
  AnimationController _likeanimationcontroller;
  Animation _iconsizeanimation;
  Animation _opacityanimation;
  Animation _radiusanimation;
  Animation _borderwidthanimation;
  Animation _iconopacityanimation;
  Animation _newiconopacityanimation;
  Animation _newiconsizeanimation;
  Animation _fadeoutborderwidthanimation;

  void likefunc() {
    print("rtrtr");
    _likeanimationcontroller.forward();
  }

  void unlikefunc() {
    _likeanimationcontroller.reverse();
    _fadeoutanimationcontroller.reverse();
  }

  @override
  void initState() {
    super.initState();
    _likeanimationcontroller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 500),
    );
    _fadeoutanimationcontroller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 100),
    );
    _iconsizeanimation = Tween(begin: 25.0, end: 10.0).animate(CurvedAnimation(
      parent: _likeanimationcontroller,
      curve: Interval(0, 0.2, curve: Curves.linear),
    ));
    _iconopacityanimation = Tween(begin: 1.0, end: 0.0).animate(CurvedAnimation(
      parent: _likeanimationcontroller,
      curve: Interval(0.2, 0.3, curve: Curves.easeIn),
    ));
    _radiusanimation = Tween(begin: 0.0, end: 40.0).animate(CurvedAnimation(
      parent: _likeanimationcontroller,
      curve: Interval(0.2, 1, curve: Curves.decelerate),
    ));
    _opacityanimation = Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
      parent: _likeanimationcontroller,
      curve: Interval(0.6, 0.8, curve: Curves.easeIn),
    ));
    _newiconopacityanimation =
        Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
      parent: _likeanimationcontroller,
      curve: Interval(0.4, 1, curve: Curves.fastOutSlowIn),
    ));
    _newiconsizeanimation =
        Tween(begin: 10.0, end: 25.0).animate(CurvedAnimation(
      parent: _likeanimationcontroller,
      curve: Interval(0.3, 1, curve: Curves.slowMiddle),
    ));
    _borderwidthanimation =
        Tween(begin: 40.0, end: 0.0).animate(CurvedAnimation(
      parent: _likeanimationcontroller,
      curve: Interval(0.2, 1, curve: Curves.easeIn),
    ));
    _fadeoutborderwidthanimation =
        Tween(begin: 1.0, end: 0.0).animate(CurvedAnimation(
      parent: _fadeoutanimationcontroller,
      curve: Curves.easeIn,
    ));
    _borderwidthanimation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _fadeoutanimationcontroller.forward();
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    _likeanimationcontroller?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _likeanimationcontroller,
      builder: (context, child) {
        return SizedBox(
          width: 50,
          height: 50,
          child: Stack(
            alignment: AlignmentDirectional.center,
            fit: StackFit.passthrough,
            overflow: Overflow.clip,
            children: <Widget>[
              FadeTransition(
                opacity: _fadeoutborderwidthanimation,
                child: Container(
                  width: _radiusanimation.value,
                  height: _radiusanimation.value,
                  decoration: BoxDecoration(
                    color: Colors.green.withOpacity(0.0),
                    shape: BoxShape.circle,
                    border: Border.all(
                        color:
                            widget.color.withOpacity(_opacityanimation.value),
                        style: BorderStyle.solid,
                        width: _borderwidthanimation.value),
                  ),
                ),
              ),
              GestureDetector(
                onTap: unlikefunc,
                child: Icon(widget.icon,
                    color: widget.color
                        .withOpacity(_newiconopacityanimation.value),
                    size: _newiconsizeanimation.value),
              ),
              GestureDetector(
                onTap: likefunc,
                child: Icon(widget.icon,
                    color:
                        Colors.white.withOpacity(_iconopacityanimation.value),
                    size: _iconsizeanimation.value),
              )
            ],
          ),
        );
      },
    );
  }
}
