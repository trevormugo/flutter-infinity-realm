import 'package:flutter/material.dart';
import '../appbaronly.dart';

class ImageView extends StatelessWidget {
  ImageView(
      {@required this.background,
      @required this.relevantcontext,
      @required this.tagging});
  final String background;
  final String relevantcontext;
  final String tagging;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(60),
          child: AppBarWidget(relevantcontext)),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: Center(
          child: Hero(
            tag: tagging,
            child: Image.network(background),
          ),
        ),
      ),
    );
  }
}
