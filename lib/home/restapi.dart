import 'dart:convert';
import 'dart:async';
import 'package:path/path.dart';

import 'package:http/http.dart' as http;
import '../ip.dart';
import '../models/uploadmodel.dart';
import '../models/curatedobj.dart';

class RestApi {
  //get tabs data
  Future<List> getdata(String t) async {
    String endpoint;
    (t == "Accounts")
        ? endpoint = '/adminreqaccounts'
        : (t == "Beats")
            ? endpoint = '/adminreqbeats'
            : (t == "Uploads")
                ? endpoint = '/adminrequploads'
                : (t == "Reports")
                    ? endpoint = '/adminreqreports'
                    : (t == "Fake Reports")
                        ? endpoint = '/adminrequploads'
                        : (t == "Fakers")
                            ? endpoint = '/adminrequploads'
                            : (t == "Top Accounts")
                                ? endpoint = '/adminreqaccounts'
                                : (t == "Subscription Plans")
                                    ? endpoint = '/pln'
                                    : endpoint = '/adminrequploads';

    http.Response data = await http.get(Adress.myip + endpoint);
    var decode = json.decode(data.body);
    List testdata = [];
    for (var i in decode) {
      testdata.add(i);
    }
    return testdata;
  }
}

class PlayerValues {
  //get upload data values
  Future getdata(String endpoint) async {
    http.Response data = await http.get(Adress.myip + endpoint);
    var decode = json.decode(data.body);
    print("$decode");
    return decode;
  }
}

class UploadItemsquery {
  //Get already existing playlists or albumns
  Future getdata(String endpoint) async {
    http.Response data = await http.get(Adress.myip + endpoint);
    var decode = json.decode(data.body);
    List testdata = [];
    for (var i in decode) {
      testdata.add(i);
    }
    return testdata;
  }

  //Upload single file
  Future uploadsingle(OneUploadRequest singleupload) async {
    var req = http.MultipartRequest(
        'POST', Uri.parse(Adress.myip + "/uploads/musicfileupload"));
    req.fields["Displayname"] = singleupload.displayname;
    req.fields["Uploader"] = singleupload.uploader;
    req.fields["Downloadable"] = singleupload.downloadable;
    req.fields["Private"] = singleupload.private;
    req.files.add(await http.MultipartFile.fromPath(
      'Musicfile',
      singleupload.musicfile,
      filename: basename(singleupload.musicfile),
    ));
    req.files.add(await http.MultipartFile.fromPath(
      'Thumbnail',
      singleupload.thumbnail,
      filename: basename(singleupload.thumbnail),
    ));

    var res = await req.send();
    return res.statusCode;
  }

  //create new albumn or playlist
  Future createnew(CuratedObj objecttocreate) async {
    var req = http.MultipartRequest(
        'POST', Uri.parse(Adress.myip + "/uploads/createcuratedobject"));
    req.fields["Name"] = objecttocreate.displayname;
    req.files.add(await http.MultipartFile.fromPath(
      'Thumbnail',
      objecttocreate.thumbnail,
      filename: basename(objecttocreate.thumbnail),
    ));
    req.fields["Curator"] = objecttocreate.curator;
    req.fields["Type"] = objecttocreate.type;
    var res = await req.send();
    return res.statusCode;
  }

  //insert into playlist or album
  Future<List<int>> insertintoplaylist(
      MultiUploadRequest uploadobj, String id, String type) async {
    var req = http.MultipartRequest(
        'POST', Uri.parse(Adress.myip + "/uploads/insertintocuratedobj"));

    List response = await Future.wait(uploadobj.files.map((item) async {
      req.fields["CurationId"] = id;
      req.fields["Type"] = type;
      req.fields["Displayname"] = item.displayname;
      req.fields["Uploader"] = item.uploader;
      req.fields["Downloadable"] = item.downloadable;
      req.fields["Private"] = item.private;
      req.files.add(await http.MultipartFile.fromPath(
        'Musicfile',
        item.musicfile,
        filename: basename(item.musicfile),
      ));
      req.files.add(await http.MultipartFile.fromPath(
        'Thumbnail',
        item.thumbnail,
        filename: basename(item.thumbnail),
      ));
      req.send();
    }));
    /*req.fields["CurationId"] = id;
    req.fields["Type"] = type;
    req.fields["Displayname"] = singleupload.displayname;
    req.fields["Uploader"] = singleupload.uploader;
    req.fields["Downloadable"] = singleupload.downloadable;
    req.fields["Private"] = singleupload.private;
    req.files.add(await http.MultipartFile.fromPath(
      'Musicfile',
      singleupload.musicfile,
      filename: basename(singleupload.musicfile),
    ));
    req.files.add(await http.MultipartFile.fromPath(
      'Thumbnail',
      singleupload.thumbnail,
      filename: basename(singleupload.thumbnail),
    ));
 
    var res = await req.send();
    return res.statusCode;
 */
    return response.map((response) {
      return response.statusCode;
    }).toList();
  }
}
