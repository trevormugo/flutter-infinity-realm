import 'package:flutter/material.dart';
import 'package:shape_of_view/shape_of_view.dart';
import 'package:line_icons/line_icons.dart';

import './customlisttile.dart';

class DrawerWidget extends StatelessWidget {
  DrawerWidget({@required this.callback});
  final Function callback;
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          ShapeOfView(
            shape: ArcShape(
              direction: ArcDirection.Outside,
              height: 20,
              position: ArcPosition.Bottom,
            ),
            elevation: 5,
            child: UserAccountsDrawerHeader(
              decoration: BoxDecoration(color: Colors.orange.withOpacity(0.5)),
              arrowColor: Colors.white,
              currentAccountPicture: CircleAvatar(
                radius: 10,
                backgroundColor: Colors.white,
              ),
              accountName: Text(
                "Trevor Lawrence",
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'NanumGothic',
                  fontSize: 12,
                ),
              ),
              accountEmail: Text(
                "trevor.lawrence@gmail.com",
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'NanumGothic',
                  fontSize: 12,
                ),
              ),
            ),
          ),
          CustomListItem(
              icon: Icon(
                LineIcons.user,
                color: Colors.black38,
                size: 30.0,
              ),
              callback: callback,
              texttype: 'Accounts',
              count: ''),
          CustomListItem(
              icon: Icon(
                LineIcons.music,
                color: Colors.black38,
                size: 30.0,
              ),
              callback: callback,
              texttype: 'Beats',
              count: ''),
          CustomListItem(
              icon: Icon(
                LineIcons.headphones,
                color: Colors.black38,
                size: 30.0,
              ),
              callback: callback,
              texttype: 'Uploads',
              count: ''),
          CustomListItem(
              icon: Icon(
                LineIcons.bell,
                color: Colors.black38,
                size: 30.0,
              ),
              callback: callback,
              texttype: 'Reports',
              count: ''),
          CustomListItem(
              icon: Icon(
                LineIcons.bell_slash,
                color: Colors.black38,
                size: 30.0,
              ),
              callback: callback,
              texttype: 'Fake Reports',
              count: ''),
          CustomListItem(
              icon: Icon(
                Icons.done_all,
                color: Colors.black38,
                size: 30.0,
              ),
              callback: callback,
              texttype: 'Top Accounts',
              count: ''),
          CustomListItem(
              icon: Icon(
                Icons.attach_money,
                color: Colors.black38,
                size: 30.0,
              ),
              callback: callback,
              texttype: 'Top Earners',
              count: ''),
          CustomListItem(
              icon: Icon(
                LineIcons.dollar,
                color: Colors.black38,
                size: 30.0,
              ),
              callback: callback,
              texttype: 'Influential Monetizers',
              count: ''),
          CustomListItem(
              icon: Icon(
                Icons.assessment,
                color: Colors.black38,
                size: 30.0,
              ),
              callback: callback,
              texttype: 'Statistics',
              count: ''),
          CustomListItem(
              icon: Icon(
                Icons.account_balance_wallet,
                color: Colors.black38,
                size: 30.0,
              ),
              callback: callback,
              texttype: 'Balance',
              count: ''),
          CustomListItem(
              icon: Icon(
                LineIcons.map_marker,
                color: Colors.black38,
                size: 30.0,
              ),
              callback: callback,
              texttype: 'Maps',
              count: ''),
          CustomListItem(
              icon: Icon(
                LineIcons.upload,
                color: Colors.black38,
                size: 30.0,
              ),
              callback: callback,
              texttype: 'Upload Files',
              count: ''),
          CustomListItem(
              icon: Icon(
                LineIcons.money,
                color: Colors.black38,
                size: 30.0,
              ),
              callback: callback,
              texttype: 'Subscription Plans',
              count: ''),
          CustomListItem(
              icon: Icon(
                LineIcons.sign_out,
                color: Colors.black38,
                size: 30.0,
              ),
              callback: callback,
              texttype: 'Log Out',
              count: ''),
        ],
      ),
    );
  }
}
