import 'package:flutter/material.dart';

class CustomListItem extends StatefulWidget {
  const CustomListItem(
      {@required this.icon, @required this.callback , @required this.texttype, this.count});
  final Icon icon;
  final Function callback;
  final String texttype;
  final String count;
  State<StatefulWidget> createState() {
    return Custom();
  }
}

class Custom extends State<CustomListItem> {
  void _nav() {
    widget.callback(widget.texttype);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _nav,
      child: Container(
        width: double.infinity,
        height: 70,
        decoration: BoxDecoration(
          color: Colors.white,
          border: BorderDirectional(
            bottom: BorderSide(
                color: Colors.black12, style: BorderStyle.solid, width: .8),
          ),
        ),
        child: Center(
          child: Row(
            mainAxisSize: MainAxisSize.max,
            children: <Expanded>[
              Expanded(flex: 1, child: widget.icon),
              Expanded(
                flex: 2,
                child: Text(
                  widget.texttype,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                    fontFamily: 'NanumGothic',
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Text(
                  widget.count,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                    fontFamily: 'NanumGothic',
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
