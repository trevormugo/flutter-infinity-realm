import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import './listitems.dart';
import '../restapi.dart';

class ReapotrsListPage extends StatefulWidget {
  ReapotrsListPage({@required this.txt});
  final txt;
  @override
  State<StatefulWidget> createState() {
    return _ReapotrsListPageItem();
  }
}

class _ReapotrsListPageItem extends State<ReapotrsListPage> {
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: RestApi().getdata(widget.txt),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        //  print(snapshot.data.length);
        //print(snapshot.data[0]['title']);
        if (snapshot.data == null) {
          return Center(
            child: CupertinoActivityIndicator(),
          );
        } else {
          return ListPage(txt: widget.txt, item: snapshot);
        }
      },
    );
  }
}