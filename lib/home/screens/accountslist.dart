import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import './listitems.dart';
import '../restapi.dart';

class AccountsListPage extends StatefulWidget {
  AccountsListPage({@required this.txt});
  final txt;
  @override
  State<StatefulWidget> createState() {
    return _AccountsListPageItem();
  }
}

class _AccountsListPageItem extends State<AccountsListPage> {
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: RestApi().getdata(widget.txt),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.data == null) {
          return Center(
            child: CupertinoActivityIndicator(),
          );
        } else {
          return ListPage(txt: widget.txt, item: snapshot);
        }
      },
    );
  }
}
