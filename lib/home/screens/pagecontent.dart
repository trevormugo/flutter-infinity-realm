import 'package:flutter/material.dart';

class PageContent extends StatelessWidget {
  PageContent({this.txt});    
  final txt;  
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Center(
        child: Text(txt),
      ),
    );
  }
}
