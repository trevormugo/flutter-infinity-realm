import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import './listitems.dart';
import '../restapi.dart';

class TopAccountsListPage extends StatefulWidget {
  TopAccountsListPage({@required this.txt});
  final txt;
  @override
  State<StatefulWidget> createState() {
    return _TopAccountsListPageItem();
  }
}

class _TopAccountsListPageItem extends State<TopAccountsListPage> {
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: RestApi().getdata(widget.txt),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        //  print(snapshot.data.length);
        //print(snapshot.data[0]['title']);
        if (snapshot.data == null) {
          return Center(
            child: CupertinoActivityIndicator(),
          );
        } else {
          return ListPage(txt: widget.txt, item: snapshot);
        }
      },
    );
  }
}