import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import './listitems.dart';
import '../restapi.dart';

class FakeReportsList extends StatefulWidget {
  FakeReportsList({@required this.txt});
  final txt;
  @override
  State<StatefulWidget> createState() {
    return _FakeReportsListItem();
  }
}

class _FakeReportsListItem extends State<FakeReportsList> {
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: RestApi().getdata(widget.txt),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        //  print(snapshot.data.length);
        //print(snapshot.data[0]['title']);
        if (snapshot.data == null) {
          return Center(
            child: CupertinoActivityIndicator(),
          );
        } else {
          return ListPage(txt: widget.txt, item: snapshot);
        }
      },
    );
  }
}