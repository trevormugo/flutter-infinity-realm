import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import '../../../ip.dart';
import '../../imageviewer.dart';

class CuratedList extends StatefulWidget {
  CuratedList({@required this.list, @required this.type});
  final list;
  final type;

  State<StatefulWidget> createState() {
    return CuratedListInstance();
  }
}

class CuratedListInstance extends State<CuratedList>
    with TickerProviderStateMixin {
  AnimationController _listitemanimationcontroller;
  Animation _slideinAnimation;
  Animation _fadeinanimation;

  static String id;

  @override
  void initState() {
    super.initState();
    _listitemanimationcontroller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 800),
    );
    _slideinAnimation =
        Tween<Offset>(begin: Offset(1, 0), end: Offset(0, 0)).animate(
      CurvedAnimation(
        parent: _listitemanimationcontroller,
        curve: Interval(0, 1, curve: Curves.decelerate),
      ),
    );
    _fadeinanimation = Tween<double>(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: _listitemanimationcontroller,
        curve: Interval(0, 1, curve: Curves.easeIn),
      ),
    );

    _listitemanimationcontroller.forward();
  }

  @override
  void dispose() {
    super.dispose();
    _listitemanimationcontroller?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    animation(String background, String tag) {
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ImageView(
                  tagging: tag,
                  background: background,
                  relevantcontext: "rere",
                )),
      );
    }

    return ListView.builder(
      itemCount: widget.list.data.length,
      itemBuilder: (context, index) {
        final item = widget.list.data[index];
        return FadeTransition(
          opacity: _fadeinanimation,
          child: SlideTransition(
            position: _slideinAnimation,
            child: (widget.type == "playlists")
                ? ListTile(
                    onTap: () => item["Id"],
                    title: Text(item['playlistname']),
                    subtitle:
                        Text("last updated ${item['lastupdatedtimestamp']}"),
                    leading: GestureDetector(
                      onTap: () => animation(
                          Adress.myip + "/playlistthumbnailread/" + item['_id'],
                          item['_id']),
                      child: Hero(
                        tag: "testing",
                        child: Image.network(
                          Adress.myip + "/playlistthumbnailread/" + item['_id'],
                        ),
                      ),
                    ),
                    trailing: Text(item['noofitems'].toString()),
                  )
                : ListTile(
                    onTap: () => item["Id"],
                    title: Text(item['albumname']),
                    subtitle:
                        Text("last updated ${item['lastupdatedtimestamp']}"),
                    leading: GestureDetector(
                      onTap: animation(
                          Adress.myip + "/albumthumbnailread/" + item['_id'],
                          item['_id']),
                      child: Hero(
                        tag: "testing",
                        child: Image.network(
                          Adress.myip + "/albumthumbnailread/" + item['_id'],
                        ),
                      ),
                    ),
                    trailing: Text(item['noofitems']),
                  ),
          ),
        );
      },
    );
  }
}
