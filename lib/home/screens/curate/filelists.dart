import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:path/path.dart';
import 'package:file_picker/file_picker.dart';
import 'package:line_icons/line_icons.dart';

import '../../../models/uploadmodel.dart';
import './curatedlists.dart';

class FileList extends StatefulWidget {
  FileList({@required this.list, @required this.type});
  final list;
  final String type;
  State<StatefulWidget> createState() {
    return FileListInstance();
  }
}

class FileListInstance extends State<FileList> {
  List<TextEditingController> multiuploadname = List();
  File pathtocuratedlistthumbnail;

  Widget _imageplaceholder = Icon(
    LineIcons.image,
    color: Colors.white10,
  );

  static MultiUploadRequest uploadrequest;
  @override
  void initState() {
    super.initState();
    uploadrequest = MultiUploadRequest(
      files: widget.list
          .map((file) => OneUploadRequest(
              displayname: basename(file.path),
              uploader: "uploader",
              downloadable: "downloadable",
              private: "private",
              musicfile: file.path,
              thumbnail:
                  "../../../assets/images/mountains_and_wind_and__infinity_sign_by_gawrifort-d5sfj0f.jpg",
              thumbnailbox: _imageplaceholder))
          .toList(),
      curationId: "CuratedListInstance.id",
      type: widget.type,
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<File> pickthumbnail() async {
    pathtocuratedlistthumbnail = await FilePicker.getFile(
      type: FileType.custom,
      allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
    );
    return pathtocuratedlistthumbnail;
  }

  @override
  Widget build(BuildContext context) {
    return GridView.count(
      scrollDirection: Axis.vertical,
      padding: const EdgeInsets.all(20),
      crossAxisSpacing: 10,
      mainAxisSpacing: 10,
      crossAxisCount: 2,
      physics: AlwaysScrollableScrollPhysics(),
      children: List.generate(uploadrequest.files.length, (index) {
        multiuploadname.add(TextEditingController());
        final item = uploadrequest.files[index];
        int no = index + 1;
        item.displayname = multiuploadname[index].text;
        return Card(
          elevation: 2,
          child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Stack(
                    alignment: AlignmentDirectional.topStart,
                    children: <Widget>[
                      Container(
                        height: 100,
                        margin: EdgeInsets.only(top: 20),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(
                            width: 1,
                            color: Colors.white38,
                            style: BorderStyle.solid,
                          ),
                          borderRadius: BorderRadius.all(
                            Radius.circular(2),
                          ),
                        ),
                        child: GestureDetector(
                          onTap: () {
                            item.thumbnailbox = FutureBuilder(
                              future: pickthumbnail(),
                              builder: (BuildContext context,
                                  AsyncSnapshot snapshot) {
                                if (snapshot.data == null) {
                                  return Center(
                                    child: CupertinoActivityIndicator(),
                                  );
                                } else {
                                  item.thumbnail = snapshot.data.path;
                                  return Container(
                                    width: 200,
                                    height: 200,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                          image: FileImage(snapshot.data),
                                          fit: BoxFit.cover),
                                    ),
                                  );
                                }
                              },
                            );
                            setState(() {});
                          },
                          child: Center(child: item.thumbnailbox),
                        ),
                      ),
                      Text("$no"),
                    ]),
                TextField(
                  controller: multiuploadname[index]
                    ..text = basename(item.musicfile),
                  obscureText: false,
                ),
              ]),
        );
      }),
    );
  }
}
