import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:io';
import 'package:path/path.dart';
import 'package:file_picker/file_picker.dart';
import 'package:line_icons/line_icons.dart';
import './filelists.dart';
import './cardchildren.dart';
import '../../../models/uploadmodel.dart';

class UploadItemsPage extends StatefulWidget {
  UploadItemsPage({@required this.txt, @required this.footercallback});
  final String txt;
  final Function footercallback;
  @override
  State<StatefulWidget> createState() {
    return UploadItemsPagePageItem();
  }
}

class UploadItemsPagePageItem extends State<UploadItemsPage> {
  List<File> files;
  File singlefileupload;
  Widget myfuture;
  final TextEditingController singleuploadname = TextEditingController();
  PageController pageController = PageController(initialPage: 0);
  int current = 0;
  bool addedfiles = false;
  Widget typeofupload = Container();
  static OneUploadRequest singleupload;
  File pathtocuratedlistthumbnail;
  Widget _imageplaceholder = Icon(
    LineIcons.image,
    color: Colors.white70,
  );

  Future picksinglefile() async {
    singlefileupload = await FilePicker.getFile(
      type: FileType.custom,
      allowedExtensions: [
        'avi',
        'flv',
        'mp4',
        'mov',
        'wmv',
        'webm',
        'webvtt',
        'ogv',
        'wav',
        'mp3'
      ],
    );
    return singlefileupload;
  }

  Future<File> pickthumbnail() async {
    pathtocuratedlistthumbnail = await FilePicker.getFile(
      type: FileType.custom,
      allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
    );
    singleupload.thumbnail = pathtocuratedlistthumbnail.path;

    return pathtocuratedlistthumbnail;
  }

  Future<List<File>> pickmultifile() async {
    files = await FilePicker.getMultiFile(
      type: FileType.custom,
      allowedExtensions: [
        'avi',
        'flv',
        'mp4',
        'mov',
        'wmv',
        'webm',
        'webvtt',
        'ogv',
        'wav',
        'mp3'
      ],
    );
    return files;
  }

  @override
  void initState() {
    super.initState();
    myfuture = Container();
    singleupload = OneUploadRequest(
      displayname: singleuploadname.text,
      uploader: "uploader",
      downloadable: "downloadable",
      private: "private",
      musicfile: "null",
      thumbnail:
          "../../../assets/images/mountains_and_wind_and__infinity_sign_by_gawrifort-d5sfj0f.jpg",
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return PageView(
      onPageChanged: (index) {
        String type = CardChildrenInstance.typeofupload;
        if (index == 2 && addedfiles == false) {
          if (type != "Single") {
            myfuture = FutureBuilder(
              future: pickmultifile(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.data == null) {
                  return Center(
                    child: CupertinoActivityIndicator(),
                  );
                } else {
                  return FileList(
                    list: snapshot.data,
                    type: type,
                  );
                }
              },
            );
          } else {
            myfuture = FutureBuilder(
              future: picksinglefile(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.data == null) {
                  return Center(
                    child: CupertinoActivityIndicator(),
                  );
                } else {
                  return Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: 170,
                          margin: EdgeInsets.only(top: 20),
                          decoration: BoxDecoration(
                            color: Colors.blue,
                            border: Border.all(
                              width: 1,
                              color: Colors.white38,
                              style: BorderStyle.solid,
                            ),
                            borderRadius: BorderRadius.all(
                              Radius.circular(2),
                            ),
                          ),
                          child: GestureDetector(
                            onTap: () {
                              _imageplaceholder = FutureBuilder(
                                future: pickthumbnail(),
                                builder: (BuildContext context,
                                    AsyncSnapshot snapshot) {
                                  if (snapshot.data == null) {
                                    return Center(
                                      child: CupertinoActivityIndicator(),
                                    );
                                  } else {
                                    singleupload.thumbnail =
                                        snapshot.data.path;
                                    return Container(
                                      width: 200,
                                      height: 200,
                                      decoration: BoxDecoration(
                                        image: DecorationImage(
                                            image: FileImage(snapshot.data),
                                            fit: BoxFit.cover),
                                      ),
                                    );
                                  }
                                },
                              );
                              setState(() {});
                            },
                            child: Center(child: _imageplaceholder),
                          ),
                        ),
                        TextField(
                          controller: singleuploadname
                            ..text = basename(snapshot.data.path),
                          obscureText: false,
                        ),
                      ]);
                }
              },
            );
          }
          addedfiles = true;
          setState(() {});
        } else if (index == 1) {
          if (type != "Single") {
            typeofupload = CardChildren(
              type: type,
              showfab: true,
            );
            setState(() {});
          } else {
            typeofupload = Container();
            pageController.animateToPage(2,
                duration: Duration(milliseconds: 600), curve: Curves.easeInOut);
            current = index;
            setState(() {});
          }
        }
      },
      controller: pageController,
      allowImplicitScrolling: true,
      pageSnapping: true,
      scrollDirection: Axis.horizontal,
      children: <Widget>[
        CardChildren(
          type: "typeofupload",
          showfab: false,
        ),
        typeofupload,
        CardChildren(
          future: myfuture,
          showfab: true,
        ),
      ],
    );
  }
}
