import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:line_icons/line_icons.dart';

import '../../restapi.dart';
import './curatedlists.dart';
import '../../../models/curatedobj.dart';
import './uploaditems.dart';
import './filelists.dart';

class CardChildren extends StatefulWidget {
  CardChildren({this.type, @required this.showfab, this.future});
  final String type;
  final Widget future;
  final bool showfab;

  State<StatefulWidget> createState() {
    return CardChildrenInstance();
  }
}

class CardChildrenInstance extends State<CardChildren> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  final TextEditingController _curatedlistname = TextEditingController();
  static String typeofupload = "Single";
  double elevation = 3;
  File pathtocuratedlistthumbnail;
  Widget _imageplaceholder = Icon(
    LineIcons.image,
    color: Colors.white70,
  );

  void change(String newvalue) {
    typeofupload = newvalue;
    setState(() {});
  }

  Future<File> pickthumbnail() async {
    pathtocuratedlistthumbnail = await FilePicker.getFile(
      type: FileType.custom,
      allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
    );
    return pathtocuratedlistthumbnail;
  }

  void showdialog(String type) {
    showDialog(
      context: this.context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Create a new $typeofupload"),
          content: StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
            return Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  TextField(
                    controller: _curatedlistname,
                    obscureText: false,
                    decoration:
                        InputDecoration(labelText: "$typeofupload Name"),
                  ),
                  Container(
                    height: 170,
                    margin: EdgeInsets.only(top: 20),
                    decoration: BoxDecoration(
                      color: Colors.blue,
                      border: Border.all(
                        width: 1,
                        color: Colors.white38,
                        style: BorderStyle.solid,
                      ),
                      borderRadius: BorderRadius.all(
                        Radius.circular(2),
                      ),
                    ),
                    child: GestureDetector(
                      onTap: () {
                        _imageplaceholder = FutureBuilder(
                          future: pickthumbnail(),
                          builder:
                              (BuildContext context, AsyncSnapshot snapshot) {
                            if (snapshot.data == null) {
                              return Center(
                                child: CupertinoActivityIndicator(),
                              );
                            } else {
                              return Container(
                                width: 200,
                                height: 200,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: FileImage(snapshot.data),
                                      fit: BoxFit.cover),
                                ),
                              );
                            }
                          },
                        );
                        setState(() {});
                      },
                      child: Center(child: _imageplaceholder),
                    ),
                  ),
                ]);
          }),
          actions: <Widget>[
            FlatButton(
              child: Text("Create"),
              onPressed: () => createnew(type),
            ),
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void createnew(String type) async {
    if (type == "Playlist") {
      var response = await UploadItemsquery().createnew(CuratedObj(
        displayname: _curatedlistname.text,
        thumbnail: pathtocuratedlistthumbnail.path,
        curator: "Infinite",
        type: "playlist",
      ));
    } else {
      var response = await UploadItemsquery().createnew(CuratedObj(
        displayname: _curatedlistname.text,
        thumbnail: pathtocuratedlistthumbnail.path,
        curator: "Infinite",
        type: "albumn",
      ));
    }
  }

  void uploaditems(String type) async {
    if (type != "Single") {
      var response = await UploadItemsquery().insertintoplaylist(
          FileListInstance.uploadrequest,
          FileListInstance.uploadrequest.curationId,
          FileListInstance.uploadrequest.type);
      response.forEach((res) {
        if (res == 504) {
          print("Success $res");
        } else {
          print("Success $res");
        }
      });
      /*var response = await Future.wait(FileListInstance.uploadrequest.files.map(
          (item) => UploadItemsquery().insertintoplaylist(
              item,
              FileListInstance.uploadrequest.curationId,
              FileListInstance.uploadrequest.type)));
      if (response == 504) {
      } else {}*/
    } else {
      var response = await UploadItemsquery()
          .uploadsingle(UploadItemsPagePageItem.singleupload);
      if (response == 504) {
      } else {}
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(alignment: AlignmentDirectional.bottomStart, children: <
        Widget>[
      Container(
        color: Colors.white,
        child: (widget.type == "typeofupload")
            ? Card(
                elevation: elevation,
                color: Colors.orange.withOpacity(0.5),
                margin: EdgeInsets.all(20),
                child: Center(
                  child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Radio(
                                value: "Single",
                                groupValue: typeofupload,
                                activeColor: Colors.white,
                                onChanged: (change) {
                                  change(change);
                                }),
                            GestureDetector(
                              onTap: () => change("Single"),
                              child: Text(
                                "Single",
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Radio(
                                value: "playlists",
                                groupValue: typeofupload,
                                activeColor: Colors.white,
                                onChanged: (change) {
                                  change("playlists");
                                }),
                            GestureDetector(
                              onTap: () => change("playlists"),
                              child: Text(
                                "Playlist",
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Radio(
                                value: "albums",
                                groupValue: typeofupload,
                                activeColor: Colors.white,
                                onChanged: (change) {
                                  change("albums");
                                }),
                            GestureDetector(
                              onTap: () => change("albums"),
                              child: Text(
                                "Album",
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ]),
                ),
              )
            : (widget.type == "playlists")
                ? FutureBuilder(
                    future: UploadItemsquery().getdata("/getplaylists"),
                    builder: (BuildContext context, AsyncSnapshot snapshot) {
                      //print(snapshot.data[0]['title']);
                      if (snapshot.data == null) {
                        return Center(
                          child: CupertinoActivityIndicator(),
                        );
                      } else {
                        return SizedBox(
                          height: 200,
                          child: CuratedList(list: snapshot, type: "playlists"),
                        );
                      }
                    },
                  )
                : (widget.type == "albums")
                    ? FutureBuilder(
                        future: UploadItemsquery().getdata("/getalbums"),
                        builder:
                            (BuildContext context, AsyncSnapshot snapshot) {
                          //print(snapshot.data[0]['title']);
                          if (snapshot.data == null) {
                            return Center(
                              child: CupertinoActivityIndicator(),
                            );
                          } else {
                            return SizedBox(
                              height: 200,
                              child:
                                  CuratedList(list: snapshot, type: "albums"),
                            );
                          }
                        },
                      )
                    : Center(
                        child: widget.future,
                      ),
      ),
      (widget.showfab == true)
          ? Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                (widget.type != "Single")
                    ? Container(
                        child: FloatingActionButton(
                          onPressed: () => showdialog(typeofupload),
                          backgroundColor: Colors.orange,
                          child: Icon(LineIcons.plus),
                          tooltip: "Create a new $typeofupload",
                          mini: true,
                        ),
                      )
                    : Container(),
                Container(
                  child: FloatingActionButton(
                    onPressed: () => uploaditems(typeofupload),
                    backgroundColor: Colors.orange,
                    child: Icon(LineIcons.upload),
                    tooltip: "Upload",
                    mini: true,
                  ),
                ),
              ],
            )
          : Container()
    ]);
  }
}
