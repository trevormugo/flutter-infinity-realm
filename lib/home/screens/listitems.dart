import 'package:flutter/material.dart';
import '../imageviewer.dart';
import '../../ip.dart';
import '../players/audio/audio.dart';
import '../players/video/video.dart';

class ListPage extends StatefulWidget {
  ListPage({@required this.txt, this.item, this.footercallback});
  final txt;
  final item;
  final Function footercallback;
  @override
  State<StatefulWidget> createState() {
    return _ListPageItem();
  }
}

class _ListPageItem extends State<ListPage> with TickerProviderStateMixin {
  AnimationController _listitemanimationcontroller;
  Animation _slideinAnimation;
  Animation _fadeinanimation;
  @override
  void initState() {
    super.initState();
    _listitemanimationcontroller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 800),
    );
    _slideinAnimation =
        Tween<Offset>(begin: Offset(1, 0), end: Offset(0, 0)).animate(
      CurvedAnimation(
        parent: _listitemanimationcontroller,
        curve: Interval(0, 1, curve: Curves.decelerate),
      ),
    );
    _fadeinanimation = Tween<double>(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: _listitemanimationcontroller,
        curve: Interval(0, 1, curve: Curves.easeIn),
      ),
    );

    _listitemanimationcontroller.forward();
  }

  @override
  void dispose() {
    super.dispose();
    _listitemanimationcontroller?.dispose();
  }

  void _tapevent(int index) {
    print(index);
    //for now only beats and uploads contain playable list tiles
    if (widget.txt == "Uploads" || widget.txt == "Beats") {
      //determine which player should be shown on the home file
      if (widget.item.data[index]['mimetype'] == "video/avi" ||
          widget.item.data[index]['mimetype'] == "video/flv" ||
          widget.item.data[index]['mimetype'] == "video/mp4" ||
          widget.item.data[index]['mimetype'] == "video/mov" ||
          widget.item.data[index]['mimetype'] == "video/wmv" ||
          widget.item.data[index]['mimetype'] == "video/webm" ||
          widget.item.data[index]['mimetype'] == "video/webvtt" ||
          widget.item.data[index]['mimetype'] == "video/ogt") {
        return widget.footercallback(
            VideoPlayerFoot(
              id: widget.item.data[index]['_id'],
              videoname: widget.item.data[index]['display'],
              uploaderid: widget.item.data[index]['uploader2'],
              timestamp: widget.item.data[index]['timestamp'],
              views: widget.item.data[index]['views'],
            ),
            "video",
            widget.item.data[index]['_id']);
      } else {
        return widget.footercallback(
            AudioPlayerFoot(
              id: widget.item.data[index]['_id'],
              audioname: widget.item.data[index]['display'],
              uploaderid: widget.item.data[index]['uploader2'],
              timestamp: widget.item.data[index]['timestamp'],
              views: widget.item.data[index]['views'],
            ),
            "audio",
            widget.item.data[index]['_id']);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    print(widget.item.data.length);
    return ListView.builder(
      itemCount: widget.item.data.length + 1,
      itemBuilder: (context, index) {
        if (index == (widget.item.data.length + 1) - 1) {
          return SizedBox(
              width: double.infinity, height: 100, child: Container());
        } else {
          final item = widget.item.data[index];
          return FadeTransition(
            opacity: _fadeinanimation,
            child: SlideTransition(
              position: _slideinAnimation,
              child: Dismissible(
                key: UniqueKey(),
                onDismissed: (direction) {
                  print(widget.item.data);
                  setState(() {
                    widget.item.data.removeAt(index);
                  });
                },
                background: Container(
                  padding: EdgeInsets.only(left: 12),
                  alignment: Alignment.centerLeft,
                  color: Colors.green,
                  child: Row(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(right: 8.0),
                        child: (widget.txt == "Accounts")
                            ? Text(
                                "report",
                                style: TextStyle(color: Colors.white),
                              )
                            : (widget.txt == "Beats")
                                ? Text(
                                    "delete",
                                    style: TextStyle(color: Colors.white),
                                  )
                                : (widget.txt == "Uploads")
                                    ? Text(
                                        "report",
                                        style: TextStyle(color: Colors.white),
                                      )
                                    : (widget.txt == "Reports")
                                        ? Text(
                                            "restore",
                                            style:
                                                TextStyle(color: Colors.white),
                                          )
                                        : (widget.txt == "Fake Reports")
                                            ? Text(
                                                "remove",
                                                style: TextStyle(
                                                    color: Colors.white),
                                              )
                                            : (widget.txt == "Top Earners")
                                                ? Text(
                                                    "verify",
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  )
                                                : (widget.txt ==
                                                        "Subscription Plans")
                                                    ? Text(
                                                        "delete",
                                                        style: TextStyle(
                                                            color:
                                                                Colors.white),
                                                      )
                                                    : Text(
                                                        "verify",
                                                        style: TextStyle(
                                                            color:
                                                                Colors.white),
                                                      ),
                      ),
                      Icon(
                        Icons.check,
                        color: Colors.white,
                      ),
                    ],
                  ),
                ),
                secondaryBackground: Container(
                  padding: EdgeInsets.only(right: 12),
                  alignment: Alignment.centerRight,
                  color: Colors.red,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(right: 8.0),
                        child: (widget.txt == "Accounts")
                            ? Text(
                                "Block",
                                style: TextStyle(color: Colors.white),
                              )
                            : (widget.txt == "Beats")
                                ? Text(
                                    "Delete",
                                    style: TextStyle(color: Colors.white),
                                  )
                                : (widget.txt == "Uploads")
                                    ? Text(
                                        "Delete",
                                        style: TextStyle(color: Colors.white),
                                      )
                                    : (widget.txt == "Reports")
                                        ? Text(
                                            "Delete Upload",
                                            style:
                                                TextStyle(color: Colors.white),
                                          )
                                        : (widget.txt == "Fake Reports")
                                            ? Text(
                                                "Block Account",
                                                style: TextStyle(
                                                    color: Colors.white),
                                              )
                                            : (widget.txt == "Top Earners")
                                                ? Text(
                                                    "Remove",
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  )
                                                : (widget.txt ==
                                                        "Subscription Plans")
                                                    ? Text(
                                                        "delete",
                                                        style: TextStyle(
                                                            color:
                                                                Colors.white),
                                                      )
                                                    : Text(
                                                        "Remove",
                                                        style: TextStyle(
                                                            color:
                                                                Colors.white),
                                                      ),
                      ),
                      Icon(
                        Icons.cancel,
                        color: Colors.white,
                      ),
                    ],
                  ),
                ),
                child: (widget.txt == "Accounts")
                    ? ListTile(
                        leading: Icon(Icons.swap_horiz),
                        title: Text(item['fullName']),
                        subtitle: Text(item['userName']),
                        trailing: GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => ImageView(
                                    tagging: item['_id'],
                                    background: Adress.myip +
                                        "/profilenoneupload/" +
                                        item['_id'],
                                    relevantcontext: item['userName']),
                              ),
                            );
                          },
                          child: CircleAvatar(
                            radius: 25,
                            backgroundImage: NetworkImage(Adress.myip +
                                "/profilenoneupload/" +
                                item['_id']),
                            backgroundColor: Colors.transparent,
                          ),
                        ),
                      )
                    : (widget.txt == "Beats")
                        ? ListTile(
                            onTap: () => _tapevent(index),
                            leading: Icon(Icons.swap_horiz),
                            title: Text(item['_id']),
                            subtitle: Text(item['path']),
                            trailing: Text(item['price']),
                          )
                        : (widget.txt == "Uploads")
                            ? ListTile(
                                onTap: () => _tapevent(index),
                                leading: Icon(Icons.swap_horiz),
                                title: Text(item['display']),
                                subtitle: Text(item['path']),
                                trailing: GestureDetector(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => ImageView(
                                            tagging: item['_id'],
                                            background: Adress.myip +
                                                "/coverimage/" +
                                                item['_id'],
                                            relevantcontext: item['display']),
                                      ),
                                    );
                                  },
                                  child: CircleAvatar(
                                    radius: 25,
                                    backgroundImage: NetworkImage(Adress.myip +
                                        "/coverimage/" +
                                        item['_id']),
                                    backgroundColor: Colors.transparent,
                                  ),
                                ),
                              )
                            : (widget.txt == "Reports")
                                ? ListTile(
                                    leading: Icon(Icons.swap_horiz),
                                    title: Text(item['uploadreportedid']),
                                    subtitle: Text(item['uploadreportedid']),
                                    trailing: GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => ImageView(
                                                tagging: item["_id"],
                                                background: Adress.myip +
                                                    "/coverimage/" +
                                                    item['uploadreportedid'],
                                                relevantcontext:
                                                    "id of the upload " +
                                                        item[
                                                            'uploadreportedid']),
                                          ),
                                        );
                                      },
                                      child: CircleAvatar(
                                        radius: 25,
                                        backgroundImage: NetworkImage(
                                            Adress.myip +
                                                "/coverimage/" +
                                                item['uploadreportedid']),
                                        backgroundColor: Colors.transparent,
                                      ),
                                    ),
                                  )
                                : (widget.txt == "Fake Reports")
                                    ? ListTile(
                                        leading: Icon(Icons.swap_horiz),
                                        title: Text(item['_id']),
                                        subtitle: Text(item['_id']),
                                      )
                                    : (widget.txt == "Fakers")
                                        ? ListTile(
                                            leading: Icon(Icons.swap_horiz),
                                            title: Text(item['_id']),
                                            subtitle: Text(item['_id']),
                                          )
                                        : (widget.txt == "Top Accounts")
                                            ? ListTile(
                                                leading: Icon(Icons.swap_horiz),
                                                title: Text(item['_id']),
                                                subtitle: Text(item['_id']),
                                              )
                                            : (widget.txt ==
                                                    "Subscription Plans")
                                                ? ListTile(
                                                    leading:
                                                        Icon(Icons.swap_horiz),
                                                    title:
                                                        Text(item['fullName']),
                                                    subtitle:
                                                        Text(item['userName']),
                                                  )
                                                : ListTile(
                                                    leading:
                                                        Icon(Icons.swap_horiz),
                                                    title: Text(item['_id']),
                                                    subtitle: Text(item['_id']),
                                                  ),
              ),
            ),
          );
        }
      },
    );
  }
}
