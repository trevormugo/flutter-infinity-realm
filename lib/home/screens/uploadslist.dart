import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import './listitems.dart';
import '../restapi.dart';

class UploadsListPage extends StatefulWidget {
  UploadsListPage({@required this.txt , @required this.footercallback});
  final String txt;
  final Function footercallback;
  @override
  State<StatefulWidget> createState() {
    return _UploadsListPageItem();
  }
}

class _UploadsListPageItem extends State<UploadsListPage> {
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: RestApi().getdata(widget.txt),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        //  print(snapshot.data.length);
        //print(snapshot.data[0]['title']);
        if (snapshot.data == null) {
          return Center(
            child: CupertinoActivityIndicator(),
          );
        } else {
          return ListPage(txt: widget.txt, item: snapshot , footercallback: widget.footercallback);
        }
      },
    );
  }
}