import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import '../restapi.dart';

class IncrementValues extends StatefulWidget {
  IncrementValues({@required this.texttoshow, @required this.text});
  final String texttoshow;
  final String text;

  State<StatefulWidget> createState() {
    return IncrementValuesInstance();
  }
}

class IncrementValuesInstance extends State<IncrementValues> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Text("${widget.text}");
  }
}
