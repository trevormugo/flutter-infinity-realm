import 'package:video_player/video_player.dart';
import 'package:flutter/material.dart';
import '../../../ip.dart';

class VideoApp extends StatefulWidget {
  VideoApp({this.srcid, this.playerWidth, this.playerHeight});
  final String srcid;
  final double playerWidth;
  final double playerHeight;
  @override
  State<StatefulWidget> createState() => VideoAppState();
}

class VideoAppState extends State<VideoApp> {
  static VideoPlayerController playerController;
  static VoidCallback listener;

  @override
  void initState() {
    super.initState();
    createVideo(Adress.myip + "/reader/" + widget.srcid);
    listener = () {
      setState(() {});
    };
  }

  static void createVideo(String route) {
    //if (playerController == null) {
    playerController = VideoPlayerController.network(route)
      ..addListener(listener)
      ..setVolume(1.0)
      ..initialize()
      ..play();
    /*} else {
      if (playerController.value.isPlaying) {
        playerController.pause();
      } else {
        playerController.initialize();
        playerController.play();
      }
    }*/
  }
  /* 
  Future<void> rebuild(String rebuildwithroute) async {
    if (playerController == null) {
      createVideo(rebuildwithroute);
    } else {
      final oldController = playerController;
      WidgetsBinding.instance.addPostFrameCallback((_) async {
        await oldController.dispose();
        setState(() {
          createVideo(rebuildwithroute);
        });
      });
      setState(() {
        playerController = null;
        //srcid = rebuildwithroute;
      });
    }
  }
  */ 
  static void updateplayer(String route) {
    //if (playerController != null) {
      playerController.dispose();
      playerController = null;
      createVideo(route);
    //}
  }

  @override
  void dispose() {
    super.dispose();
    playerController.dispose();
    playerController = null;
    listener = null;
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: widget.playerWidth,
      height: widget.playerHeight,
      child: AspectRatio(
        aspectRatio: 16 / 9,
        child: VideoPlayer(playerController),
      ),
    );
  }
}
