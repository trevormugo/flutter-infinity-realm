import 'package:flutter/material.dart';
import './fullscreenvideo.dart';
import '../playermanager.dart';
import './actuallvideotwo.dart';

class VideoPlayerFoot extends StatefulWidget {
  VideoPlayerFoot(
      {@required this.id,
      @required this.videoname,
      @required this.uploaderid,
      @required this.timestamp,
      @required this.views});
  final String id;
  final String videoname;
  //use uploader 2 coz its a string value
  final String uploaderid;
  final String timestamp;
  final int views;
  State<StatefulWidget> createState() {
    return _VideoPlayer();
  }
}

class _VideoPlayer extends State<VideoPlayerFoot>
    with TickerProviderStateMixin {
  AnimatedIconData icon = AnimatedIcons.pause_play;
  AnimationController _iconanimate;
  void _animatepauseplay() {
    if (!VideoAppState.playerController.value.isPlaying) {
      _iconanimate.reverse();
      VideoAppState.playerController.play();
    } else {
      _iconanimate.forward();
      VideoAppState.playerController.pause();
    }
  }

  void initState() {
    super.initState();
    _iconanimate = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 500),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _iconanimate?.dispose();
  }

  Widget build(BuildContext context) {
    animation() {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => FullScreenVideoPlayer(
            songname: widget.videoname,
            id: widget.id,
            views: widget.views,
          ),
        ),
      );
    }

    return Container(
      color: Colors.white.withOpacity(0.9),
      child: ListTile(
        leading: Container(
          child: GestureDetector(
            onTap: animation,
            child: SizedBox(
              width: 100,
              height: double.infinity,
              child: VideoApp(
                srcid: widget.id,
                playerHeight: double.infinity,
                playerWidth: 100,
              ),
            ),
          ),
        ),
        title: Text(widget.videoname,
            style: TextStyle(
                fontSize: 14, color: Colors.black38, fontFamily: 'NanumGothic'),
            textAlign: TextAlign.center),
        trailing: GestureDetector(
          onTap: _animatepauseplay,
          child: AnimatedIcon(
            icon: icon,
            progress: _iconanimate,
            color: Colors.black38,
          ),
        ),
      ),
    );
  }
}
