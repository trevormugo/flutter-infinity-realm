import 'package:flutter/material.dart';
import 'dart:ui';
import 'package:line_icons/line_icons.dart';
import '../../animations/iconanimation.dart';
import './actuallvideotwo.dart';
import '../playermanager.dart';
import '../retrievetext.dart';

class FullScreenVideoPlayer extends StatefulWidget {
  FullScreenVideoPlayer(
      {@required this.songname, @required this.id, @required this.views});
  final String songname;
  final String id;
  final int views;
  @override
  State<StatefulWidget> createState() {
    return _FullVideoPlayer();
  }
}

class _FullVideoPlayer extends State<FullScreenVideoPlayer> {
  double _value = 1.0;
  bool isplaying = VideoAppState.playerController.value.isPlaying;
  void _toggle() {
    if (!VideoAppState.playerController.value.isPlaying) {
      PlayerManager().play("video");
    } else {
      PlayerManager().pause("video");
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          title: Text(widget.songname,
              style: TextStyle(color: Colors.white, fontFamily: 'NanumGothic'),
              textAlign: TextAlign.center),
          backgroundColor: Colors.black12.withOpacity(0.5),
          elevation: 0.0,
          actions: <Widget>[Icon(LineIcons.ellipsis_v)],
        ),
      ),
      extendBodyBehindAppBar: true,
      extendBody: true,
      body: Stack(
        alignment: AlignmentDirectional.center,
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            color: Colors.black,
            height: double.infinity,
            width: double.infinity,
            child: VideoApp(
              srcid: widget.id,
              playerWidth: 640,
              playerHeight: 360,
            ),
          ),
          Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              gradient: LinearGradient(
                begin: FractionalOffset.topCenter,
                end: FractionalOffset.bottomCenter,
                colors: [
                  Colors.transparent,
                  Colors.black.withOpacity(0.9),
                ],
                stops: [0.0, 1.0],
              ),
            ),
          ),
          AnimatedOpacity(
            opacity: isplaying ? 1.0 : 0.0,
            duration: Duration(milliseconds: 500),
            child: BackdropFilter(
              filter: ImageFilter.blur(
                sigmaX: 9.0,
                sigmaY: 9.0,
              ),
              child: GestureDetector(
                onTap: _toggle,
                child: Container(
                  height: double.infinity,
                  width: double.infinity,
                  color: Colors.black.withOpacity(0.8),
                  child: Center(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          LineIcons.angle_left,
                          color: Colors.white,
                          size: 40.0,
                        ),
                        Icon(
                          LineIcons.play,
                          color: Colors.white,
                          size: 40.0,
                        ),
                        Icon(
                          LineIcons.angle_right,
                          color: Colors.white,
                          size: 40.0,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
          Container(
            width: double.infinity,
            height: double.infinity,
            margin: EdgeInsets.only(top: 100),
            child: Align(
              alignment: Alignment.topLeft,
              child: Container(
                padding: EdgeInsets.all(5),
                color: Colors.black.withOpacity(0.5),
                child: Text(
                  "Artist Name",
                  softWrap: true,
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
            ),
          ),
        ],
      ),
      bottomNavigationBar: Container(
        width: double.infinity,
        height: 180,
        color: Colors.white.withOpacity(0.0),
        margin: EdgeInsets.only(top: 0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              child: Slider(
                  value: _value,
                  min: 0,
                  max: 100,
                  inactiveColor: Colors.black26,
                  activeColor: Colors.orange[400],
                  onChanged: (double value) {
                    setState(() {
                      _value = value;
                    });
                  }),
            ),
            Center(
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      IconAnimation(
                          color: Colors.green,
                          icon: LineIcons.headphones,
                          shouldanimate: false),
                      RetrieveText(
                        texttoshow: "views",
                        uploadid: widget.id,
                      ),
                    ],
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      IconAnimation(
                          color: Colors.red,
                          icon: LineIcons.heart,
                          shouldanimate: true),
                      RetrieveText(
                        texttoshow: "likes",
                        uploadid: widget.id,
                      ),
                    ],
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      IconAnimation(
                          color: Colors.orange,
                          icon: LineIcons.comments,
                          shouldanimate: true),
                      RetrieveText(
                        texttoshow: "comments",
                        uploadid: widget.id,
                      ),
                    ],
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      IconAnimation(
                          color: Colors.white,
                          icon: LineIcons.share,
                          shouldanimate: true),
                      RetrieveText(
                        texttoshow: "shares",
                        uploadid: widget.id,
                      ),
                    ],
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Icon(LineIcons.bars, color: Colors.white, size: 25),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
