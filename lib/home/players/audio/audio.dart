import 'package:flutter/material.dart';
import 'package:flutter_exoplayer/audioplayer.dart';
import './fullscreenaudio.dart';
import '../../../ip.dart';
import './actuallaudioplayer.dart';
import '../playermanager.dart';

class AudioPlayerFoot extends StatefulWidget {
  AudioPlayerFoot(
      {@required this.id,
      @required this.audioname,
      @required this.uploaderid,
      @required this.timestamp,
      @required this.views});
  final String id;
  final String audioname;
  //use uploader 2 coz its a string value
  final String uploaderid;
  final String timestamp;
  final int views;
  State<StatefulWidget> createState() {
    return AudioPlayerInstance();
  }
}

class AudioPlayerInstance extends State<AudioPlayerFoot>
    with TickerProviderStateMixin {
  AnimatedIconData icon = AnimatedIcons.pause_play;
  AnimationController _animatedicon;
  void _animatepauseplay() {
    if (AudioInstance.isPlaying) {
      _animatedicon.forward();
      AudioInstance.pause();
    } else {
      //youre pausing show play icon
      _animatedicon.reverse();
      AudioInstance.resume();
    }
  }

  @override
  void initState() {
    super.initState();
    _animatedicon = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 500),
    );
  }

  @override
  void dispose() async {
    _animatedicon?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    animation() {
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => FullScreenAudioPlayer(
                  songname: widget.audioname,
                  image: Adress.myip + "/coverimage/" + widget.id,
                  id: widget.id,
                )),
      );
    }

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        ActuallAudio(
          srcid: widget.id,
        ),
        Container(
          color: Colors.white.withOpacity(0.9),
          child: ListTile(
            leading: Container(
              child: GestureDetector(
                onTap: animation,
                child: Hero(
                  tag: "testing",
                  child: Image.network(
                    Adress.myip + "/coverimage/" + widget.id,
                  ),
                ),
              ),
            ),
            title: Text(widget.audioname,
                style: TextStyle(
                    fontSize: 14,
                    color: Colors.black38,
                    fontFamily: 'NanumGothic'),
                textAlign: TextAlign.center),
            trailing: GestureDetector(
              onTap: _animatepauseplay,
              child: AnimatedIcon(
                icon: icon,
                progress: _animatedicon,
                color: Colors.black38,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
