import 'dart:ui';
import 'package:line_icons/line_icons.dart';
import 'package:flutter/material.dart';
import '../../animations/iconanimation.dart';
import './actuallaudioplayer.dart';
import '../playermanager.dart';
import '../retrievetext.dart';

class FullScreenAudioPlayer extends StatefulWidget {
  FullScreenAudioPlayer(
      {@required this.songname, @required this.image, @required this.id});
  final String songname;
  final String image;
  final String id;
  @override
  State<StatefulWidget> createState() {
    return _FullAudioPlayer();
  }
}

class _FullAudioPlayer extends State<FullScreenAudioPlayer> {
  double _value;
  bool _visible;

  void _toggle() {
    if (AudioInstance.isPlaying) {
      //youre palying show pause icon
      AudioInstance.pause();
      setState(() {});
    } else {
      //youre pausing show play icon
      AudioInstance.resume();
      setState(() {});
    }
  }

  @override
  void initState() {
    super.initState();
    _value = AudioInstance.progress;
    _visible = AudioInstance.isPlaying;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          title: Text(widget.songname,
              style: TextStyle(color: Colors.white, fontFamily: 'NanumGothic'),
              textAlign: TextAlign.center),
          backgroundColor: Colors.black12.withOpacity(0.5),
          elevation: 0.0,
          actions: <Widget>[Icon(LineIcons.ellipsis_v)],
        ),
      ),
      extendBodyBehindAppBar: true,
      extendBody: true,
      body: Stack(
        alignment: AlignmentDirectional.center,
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: NetworkImage(widget.image), fit: BoxFit.cover),
            ),
          ),
          Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              gradient: LinearGradient(
                begin: FractionalOffset.topCenter,
                end: FractionalOffset.bottomCenter,
                colors: [
                  Colors.transparent,
                  Colors.black.withOpacity(0.9),
                ],
                stops: [0.0, 1.0],
              ),
            ),
          ),
          AnimatedOpacity(
            opacity: _visible ? 1.0 : 0.0,
            duration: Duration(milliseconds: 500),
            child: BackdropFilter(
              filter: ImageFilter.blur(
                sigmaX: 9.0,
                sigmaY: 9.0,
              ),
              child: GestureDetector(
                onTap: _toggle,
                child: Container(
                  height: double.infinity,
                  width: double.infinity,
                  color: Colors.black.withOpacity(0.8),
                  child: Center(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          LineIcons.angle_left,
                          color: Colors.white,
                          size: 40.0,
                        ),
                        Icon(
                          LineIcons.play,
                          color: Colors.white,
                          size: 40.0,
                        ),
                        Icon(
                          LineIcons.angle_right,
                          color: Colors.white,
                          size: 40.0,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
          Container(
            width: double.infinity,
            height: double.infinity,
            margin: EdgeInsets.only(top: 100),
            child: Align(
              alignment: Alignment.topLeft,
              child: Container(
                padding: EdgeInsets.all(5),
                color: Colors.black.withOpacity(0.5),
                child: Text(
                  "Artist Name",
                  softWrap: true,
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
            ),
          ),
        ],
      ),
      bottomNavigationBar: Container(
        width: double.infinity,
        height: 180,
        color: Colors.white.withOpacity(0.0),
        margin: EdgeInsets.only(top: 0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              child: Slider(
                  value: _value,
                  min: 0,
                  max: 100,
                  inactiveColor: Colors.black26,
                  activeColor: Colors.orange[400],
                  onChanged: (double value) {
                    setState(() {
                      _value = value;
                    });
                  }),
            ),
            Center(
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      IconAnimation(
                          color: Colors.green,
                          icon: LineIcons.headphones,
                          shouldanimate: false),
                      RetrieveText(
                        texttoshow: "views",
                        uploadid: widget.id,
                      ),
                    ],
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      IconAnimation(
                          color: Colors.red,
                          icon: LineIcons.heart,
                          shouldanimate: true),
                      RetrieveText(
                        texttoshow: "views",
                        uploadid: widget.id,
                      ),
                    ],
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      IconAnimation(
                          color: Colors.orange,
                          icon: LineIcons.comments,
                          shouldanimate: true),
                      RetrieveText(
                        texttoshow: "views",
                        uploadid: widget.id,
                      ),
                    ],
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      IconAnimation(
                          color: Colors.white,
                          icon: LineIcons.share,
                          shouldanimate: true),
                      RetrieveText(
                        texttoshow: "views",
                        uploadid: widget.id,
                      ),
                    ],
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Icon(LineIcons.bars, color: Colors.white, size: 25),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
