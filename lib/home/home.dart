import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import '../appbaronly.dart';
import './drawer.dart';
import './screens/pagecontent.dart';
import './screens/accountslist.dart';
import './screens/beatslist.dart';
import './screens/uploadslist.dart';
import './screens/reportslist.dart';
import './screens/fakereportslist.dart';
import './screens/topaccountslist.dart';
import './screens/topearnerslist.dart';
import './screens/influencialmonetizerslist.dart';
import './screens/subscriptionplan.dart';
import './screens/curate/uploaditems.dart';
import '../ip.dart';
import './players/video/actuallvideotwo.dart';
import './players/audio/actuallaudioplayer.dart';

class Home extends StatefulWidget {
  Home(this.txt);
  final String txt;
  State<StatefulWidget> createState() {
    return HomePage(txt);
  }
}

class HomePage extends State<Home> {
  HomePage(this.t);
  String t;
  void callback(String y) {
    setState(() {
      t = y;
    });
  }

  Widget _showing;
  @override
  void initState() {
    super.initState();
    _showing = Container();
    //our manager
  }

  @override
  void dispose() async {
    super.dispose();
  }

  String typeofwidget;
  void footercallback(Widget widget, String type, String route) {
    //_streamcontrol.add(object);
    if (typeofwidget == "video" && type == "video") {
      VideoAppState.updateplayer(Adress.myip + "/reader/" + route);
      //VideoAppState.createVideo(Adress.myip + "/reader/" + route);
      typeofwidget = type;
      setState(() {
        _showing = widget;
      });
    } else if (typeofwidget == "audio" && type == "audio") {
      AudioInstance.updateplayer(route);
      //AudioInstance.startPlaying(route);
      typeofwidget = type;
      setState(() {
        _showing = widget;
      });
    } else {
      //if there was no widget initialized prior or if the widget to be initialized is not the same as the widget prior
      typeofwidget = type;
      setState(() {
        _showing = widget;
      });
    }
  }

  bool playing = true;
  void toggle() {
    setState(() {
      playing = !playing;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(60), child: AppBarWidget(t)),
      drawer: DrawerWidget(callback: callback),
      drawerScrimColor: Colors.black38,
      body: Stack(
        alignment: AlignmentDirectional.bottomCenter,
        children: <Widget>[
          (t == "Balance")
              ? PageContent(txt: t)
              : (t == "Statistics")
                  ? PageContent(txt: t)
                  : (t == "Maps")
                      ? PageContent(txt: t)
                      : (t == "Log Out")
                          ? PageContent(txt: t)
                          : (t == "Upload Files")
                              ? UploadItemsPage(
                                  txt: t, footercallback: footercallback)
                              : (t == "Accounts")
                                  ? AccountsListPage(txt: t)
                                  : (t == "Beats")
                                      ? BeatsListPage(txt: t)
                                      : (t == "Uploads")
                                          ? UploadsListPage(
                                              txt: t,
                                              footercallback: footercallback)
                                          : (t == "Reports")
                                              ? ReapotrsListPage(txt: t)
                                              : (t == "Fake Reports")
                                                  ? FakeReportsList(txt: t)
                                                  : (t == "Top Accounts")
                                                      ? TopAccountsListPage(
                                                          txt: t)
                                                      : (t == "Top Earners")
                                                          ? TopEarnersListPage(
                                                              txt: t)
                                                          : (t == "Subscription Plans")
                                                              ? SubscriptionPlanListPage(
                                                                  txt: t)
                                                              : InfluencialMonetizersListPage(
                                                                  txt: t),
          _showing,
        ],
      ),
    );
  }
}
