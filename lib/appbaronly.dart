import 'package:flutter/material.dart';

class AppBarWidget extends StatelessWidget {
  AppBarWidget(this._appbartext);
  final _appbartext;
  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(_appbartext,
          style: TextStyle(color: Colors.black38, fontFamily: 'NanumGothic'),
          textAlign: TextAlign.center),
      backgroundColor: Colors.orange.withOpacity(0.5),
      brightness: Brightness.light,
    );
  }
}
